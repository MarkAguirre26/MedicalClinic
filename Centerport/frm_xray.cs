﻿using MedicalManagement.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows.Forms;

namespace MedicalManagement
{
    public partial class frm_xray : Form, MyInter
    {
        Main fmain; public static bool NewXray; public static TextBox pin; public static TextBox LabID;
        public static string radcn;

        DataClasses1DataContext db = new DataClasses1DataContext(Properties.Settings.Default.MyConString);
        public List<QueueSearchList_Model> xrayAdd_model = new List<QueueSearchList_Model>();
        public List<Xray_Model> Xray_model = new List<Xray_Model>();
        public frm_xray(Main mainn)
        {
            InitializeComponent();
            pin = txt_Papin; LabID = txt_resultID;
            fmain = mainn;
        }

        private void cmd_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }



        public void New()
        {




        }
        public void Save()
        {
            if (NewXray)
            {
                Insert();
            }
            else
            {

                Update_Medical();


            }


        }


        void Update_Medical()
        {
            try
            {


                int norma_xray;
                if (cb_normal.Checked)
                {
                    norma_xray = 1;
                }
                else
                {
                    norma_xray = 0;
                }

                string DateResult = "";
                if (dt_result_Date.Text == "00/00/0000")
                {
                    DateResult = DateTime.Today.ToShortDateString();
                }
                else
                {
                    DateResult = dt_result_Date.Text;
                }

                db.ExecuteCommand("UPDATE t_result_main SET reference_no= {0},result_date= {1}  ,specimen_no= {2}   WHERE resultid=  {3}", txt_xrayNo.Text, DateResult, txt_speciment.Text, LabID.Text);
                db.ExecuteCommand("UPDATE t_radiology SET result_date={0},reference_no= {1} ,findings=  {2},impression= {3} ,remark={4} WHERE resultid=  {5}", DateResult, txt_xrayNo.Text, txt_findings.Text, txt_impression.Text, norma_xray, LabID.Text);

                txt_Papin.Select();
                NewXray = true;
                fmain.ts_printPreview_Xray.Enabled = true; fmain.ts_add_xray.Enabled = true; fmain.ts_edit_xray.Enabled = true; fmain.ts_delete_xray.Enabled = false; fmain.ts_save_xray.Enabled = false; fmain.ts_cancel_xray.Enabled = false; fmain.ts_search_xray.Enabled = true; fmain.ts_print_xray.Enabled = true;
                Availability(false);




            }
            catch (Exception ex)
            {
                MessageBox.Show(this, string.Format("An error occured {0}", ex.Message), Properties.Settings.Default.SystemName.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation); return;

            }


        }

        void Insert()
        {
            try
            {




                int norma_xray;
                if (cb_normal.Checked)
                {
                    norma_xray = 1;
                }
                else
                {
                    norma_xray = 0;
                }
                string DateResult = "";
                if (dt_result_Date.Text == "00/00/0000")
                {
                    DateResult = DateTime.Today.ToShortDateString();
                }
                else
                {
                    DateResult = dt_result_Date.Text;
                }


                //LabID.Clear();
                //var i = db.sp_Xray_GenerateID(variables.RadiologyFormType).FirstOrDefault();
               // LabID.Text = i.generated_id;
               //LabID.Text = System.Guid.NewGuid().ToString();

                db.ExecuteCommand("INSERT INTO t_result_main (resultid,resulttype,papin,result_date,pathologist,status,fitness_date,valid_until,remarks,recommendation,repeat_test_requestby,specimen_no,medtech,medtech_license,pathologist_license,reference_no,restriction,basic_doh_exam,add_lab_tests,flag_medlab_req,deck_srvc_flag,engine_srvc_flag,catering_srvc_flag,other_srvc_flag) values({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23})", LabID.Text, variables.RadiologyFormType, pin.Text, DateResult, "", "PENDING", "", "", "", "", "", txt_speciment.Text, "", "", "", txt_xrayNo.Text, "", "", "", "", "", "", "", "");
                db.ExecuteCommand("INSERT INTO t_radiology (resultid, result_date, papin, reference_no, findings, impression,remark,Type) VALUES ({0}, {1}, {2}, {3}, {4}, {5},{6},{7})", txt_resultID.Text, DateResult, txt_Papin.Text, txt_xrayNo.Text, txt_findings.Text, txt_impression.Text, norma_xray, variables.RadiologyFormType);
                xrayAdd_model.Clear();
                if (!backgroundWorker1.IsBusy)
                { backgroundWorker1.RunWorkerAsync(); }



                txt_Papin.Select();
                Availability(false);
                fmain.ts_printPreview_Xray.Enabled = true; fmain.ts_add_xray.Enabled = true; fmain.ts_edit_xray.Enabled = true; fmain.ts_delete_xray.Enabled = false; fmain.ts_save_xray.Enabled = false; fmain.ts_cancel_xray.Enabled = false; fmain.ts_search_xray.Enabled = true; fmain.ts_print_xray.Enabled = true;





            }
            catch (Exception ex)
            {
                MessageBox.Show(this, string.Format("An error occured {0}", ex.Message), Properties.Settings.Default.SystemName.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation); return;

            }


        }




        public void Edit()
        {
            if (fmain.UserLevel == 1 || fmain.UserLevel == 2 || fmain.UserLevel == 7 || fmain.UserLevel == 6)
            {
                NewXray = false;
                Availability(true);
                fmain.ts_add_xray.Enabled = false; fmain.ts_edit_xray.Enabled = false; fmain.ts_delete_xray.Enabled = false; fmain.ts_save_xray.Enabled = true; fmain.ts_cancel_xray.Enabled = true; fmain.ts_search_xray.Enabled = false; fmain.ts_print_xray.Enabled = false; fmain.ts_printPreview_Xray.Enabled = false;

            }
            else
            {
                if (MessageBox.Show("You do not have enough access privileges for this operation, Please use RELEASING account. \n Would you like to continue?", "Action Denied!", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) == DialogResult.Yes)
                {
                    new frm_login(fmain).ShowDialog();
                }

            }
        }
        public void Delete()
        {
            Delete_Record();
        }
        public void Cancel()
        {
            if (NewXray)
            {
                Tool.ClearFields(groupBox1);
                txt_Papin.Clear();
                ClearAll();
                Availability(false);

                fmain.ts_add_xray.Enabled = true; fmain.ts_edit_xray.Enabled = false; fmain.ts_delete_xray.Enabled = false; fmain.ts_save_xray.Enabled = false; fmain.ts_search_xray.Enabled = true; fmain.ts_print_xray.Enabled = false; fmain.ts_printPreview_Xray.Enabled = false; fmain.ts_cancel_xray.Enabled = false;

            }
            else
            {
                Availability(false);
                ClearAll();
                Search_Patient(); search_Medical(); search_Xray();
                fmain.ts_add_xray.Enabled = true; fmain.ts_edit_xray.Enabled = true; fmain.ts_delete_xray.Enabled = false; fmain.ts_save_xray.Enabled = false; fmain.ts_search_xray.Enabled = true; fmain.ts_print_xray.Enabled = true; fmain.ts_printPreview_Xray.Enabled = true; fmain.ts_cancel_xray.Enabled = false;
            }


        }
        public void Print()
        {
            string title = "";
            if (variables.RadiologyFormType == "UTZ")
            {
                title = "ULTRASOUND";
            }
            else
            {
                title = variables.RadiologyFormType;
            }


            Reporting.Write(TemplatePath.basePath + "Radiology-TEMPLATE",
                new string[] { "D11", "A14", "B15", "H14", "I14", "K17", "I18", "B22", "B30", },
                new string[] { title + " REPORT", txt_name.Text, dt_result_Date.Text, txt_gender.Text, txt_age.Text, txt_position.Text, txt_xrayNo.Text, txt_findings.Text, txt_impression.Text });





        }
        public void Search()
        {



        }

        private void frm_xray_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                if (fmain.ts_cancel_xray.Enabled == true)
                { Cancel(); }
                else
                { this.Close(); }

            }
            else if (e.KeyCode == Keys.Add && e.Modifiers == Keys.Control)
            {
                if (fmain.ts_add_xray.Enabled == true)
                {

                    fmain.add_xray();
                }
            }

            else if (e.KeyCode == Keys.S && e.Modifiers == Keys.Control)
            {
                if (fmain.ts_save_xray.Enabled == true)
                {
                    Save();

                }

            }
            else if (e.KeyCode == Keys.P && e.Modifiers == Keys.Control)
            {
                if (fmain.ts_print_xray.Enabled == true)
                {
                    Print();
                }
            }
            else if (e.KeyCode == Keys.F && e.Modifiers == Keys.Control)
            {

                if (fmain.ts_search_xray.Enabled == true)
                {
                    fmain.search_xray();
                }
            }
            else if (e.KeyCode == Keys.F4)
            {
                if (fmain.ts_edit_xray.Enabled == true)
                {
                    Edit();

                }
            }
            else if (e.KeyCode == Keys.Delete)
            {
                if (fmain.ts_delete_xray.Enabled == true)
                {

                    Delete();
                }

            }
        }

        private void overlayShadow1_Click(object sender, EventArgs e)
        {
            if (LabID.Text == "")
            {
                MessageBox.Show("Please  select patient  first!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                //MessageBox.Show("Please click EDIT BUTTON or press f4 key in your keyboard to modify!", "Message", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }



        }


        private void frm_xray_Load(object sender, EventArgs e)
        {
            //ClassSql.DbConnect();
            Cursor.Current = Cursors.Default;
            Availability(false);
            groupBox2.Text = variables.RadiologyFormType + " RESULT";
            //Load_Medical();




        }

        private void frm_xray_FormClosing(object sender, FormClosingEventArgs e)
        {
            fmain.Xray = true;
            fmain.ts_printPreview_Xray.Enabled = false; fmain.ts_add_xray.Enabled = true; fmain.ts_edit_xray.Enabled = false; fmain.ts_delete_xray.Enabled = false; fmain.ts_save_xray.Enabled = false; fmain.ts_search_xray.Enabled = true; fmain.ts_print_xray.Enabled = false; fmain.ts_cancel_xray.Enabled = false;
            //ClassSql.DbClose();
            // fmain.Strip_sub.Visible = false;
        }

        private void txt_resultID_TextChanged(object sender, EventArgs e)
        {




        }



        public void search_Xray()
        {

            try
            {



                var i = db.sp_Xray_Detail(LabID.Text).FirstOrDefault();
                if (i != null)
                {


                    lbl_Xray_Result_Cn.Tag = i.cn.ToString() ?? "-";
                    txt_xrayNo.Text = i.reference_no.ToString() ?? "-";
                    txt_findings.Text = i.findings.ToString() ?? "-";
                    txt_impression.Text = i.impression.ToString() ?? "-";
                    int impression = Convert.ToInt32(i.remark ?? 0);
                    if (impression == 1)
                    {
                        cb_normal.Checked = true;
                    }
                    else
                    {
                        cb_normal.Checked = false;
                    }


                }



            }
            catch (Exception ex)
            {
                MessageBox.Show(this, string.Format("An error occured {0}", ex.Message), Properties.Settings.Default.SystemName.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation); return;

            }


        }

        public void search_Medical()
        {

            try
            {



                var i = db.sp_Xray_Medical(LabID.Text, variables.RadiologyFormType).FirstOrDefault();
                if (i != null)
                {


                    lbl_medical_cn.Tag = i.cn.ToString() ?? "-";
                    DateTime date1 = DateTime.Parse(i.result_date.ToString() ?? "-").Date;
                    dt_result_Date.Text = date1.ToShortDateString() ?? "-";
                    txt_speciment.Text = i.specimen_no.ToString() ?? "-";

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(this, string.Format("An error occured {0}", ex.Message), Properties.Settings.Default.SystemName.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation); return;

            }


        }



        public void Search_Patient()
        {
            try
            {



                var i = db.sp_Xray_Patient(pin.Text).FirstOrDefault();
                this.txt_name.Text = i.PatientName ?? "-";
                DateTime temp1;
                if (DateTime.TryParse(i.birthdate.ToString() ?? "-", out temp1))
                {
                    dt_bday.Format = DateTimePickerFormat.Custom;
                    dt_bday.CustomFormat = "MM/dd/yyyy";

                    dt_bday.Value = Convert.ToDateTime(i.birthdate.ToString() ?? "-").Date;
                }
                else
                {

                    dt_bday.Format = DateTimePickerFormat.Custom;
                    dt_bday.CustomFormat = "00/00/0000";

                }


                this.txt_gender.Text = i.gender.ToString() ?? "-";
                this.txt_agency.Text = i.employer.ToString() ?? "-";
                txt_position.Text = i.position.ToString() ?? "-";





            }
            catch (Exception ex)
            {
                MessageBox.Show(this, string.Format("An error occured {0}", ex.Message), Properties.Settings.Default.SystemName.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation); return;

            }



        }


        void Delete_Record()
        {


        }



        public void Availability(bool bl)
        {
            //Tool.SetEnable(panel3, bl);
            //Tool.SetEnable(groupBox2, bl);
            //Tool.SetEnable(groupBox3, bl);
            //Tool.SetEnable(groupBox4, bl);



            if (bl == true)
            { overlayShadow1.Visible = false; overlayShadow1.SendToBack(); }
            else
            { overlayShadow1.Visible = true; overlayShadow1.BringToFront(); }

        }

        public void ClearAll()
        {
            Tool.ClearFields(panel3);
            Tool.ClearFields(groupBox2);
            Tool.ClearFields(groupBox3);


        }

        public void setDeafultValue()
        {
            if (variables.RadiologyFormType == "XRAY")
            {
                string xrayResultDefaultValue = "Both lung fields are clear, Heart is not enlarged, Rest of the chest findings are normal";
                string xrayRemarksDefaultValue = "Normal Chest";
                txt_findings.Text = xrayResultDefaultValue;
                txt_impression.Text = xrayRemarksDefaultValue;
            }
        }


        //public void setDefaultFiledsValue()
        //{
        //    txt_specimentNo.Text = "1012";
        //    txt_radiologist_findings.Text = "THE LUNGS ARE CLEAR.\nHEART IS NOT ENLARGED.\n\nMEDIASTINUM, DIAPHRAGM, SULCI AND OSSEOUS STRUCTURES ARE INTACT.";
        //    txt_impression.Text = "NORMAL CHEST XRAY.";
        //    IniFile ini = new IniFile(ClassSql.MMS_Path);
        //    txt_RadioLogist.Text = ini.IniReadValue("MEDICAL", "Xray_Radiologist");
        //    txt_RadioLogist_Lic.Text = ini.IniReadValue("MEDICAL", "Xray Radiologist_license");
        //    txt_XRAYtech.Text = ini.IniReadValue("MEDICAL", "XRAY_TECH");
        //    txt_XRAYtech_lic.Text = ini.IniReadValue("MEDICAL", "XRAYTECH_LICENSE");


        //}



        private void txt_Papin_TextChanged(object sender, EventArgs e)
        {

            //if (NewXray)
            //{
            //    ClassSql a = new ClassSql(); long cnt;
            //    cnt = a.CountColumn("SELECT Count(t_result_main.papin) AS `count` FROM t_result_main WHERE t_result_main.papin =  '" + Tool.ReplaceString(txt_Papin.Text) + "' and resulttype = 'UTZ'");

            //    if (cnt >= 1)
            //    {

            //        if (MessageBox.Show("Patient had previous laboratory examination! Would you like to update Hir/Her record?", Properties.Settings.Default.Systemname.ToString(), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            //        {
            //            NewXray = false;
            //            search_Medical_sub();
            //            Search_Patient();
            //            search_Xray();                 
            //            Availability(true);

            //        }
            //        else
            //        {

            //            NewXray = true;
            //            Search_Patient();
            //            Availability(true);
            //            ClearAll();


            //        }

            //    }
            //    else
            //    {

            //        NewXray = true;
            //        Search_Patient();
            //        Availability(true);
            //        ClearAll();

            //    }


            //}
            //else
            //{

            //    Search_Patient();

            //}

            //txt_Papin.Select();
            //Load_Medical();

        }

        private void txt_bday_TextChanged(object sender, EventArgs e)
        {


        }

        private void txt_medTect_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_pathologist_TextChanged(object sender, EventArgs e)
        {

        }

        private void txt_radiologist_TextChanged(object sender, EventArgs e)
        {

            //ClassSql a = new ClassSql();
            //a.PutDataTOTextBox("SELECT tbl_medical.cn, tbl_medical.Name, tbl_medical.License, tbl_medical.Type FROM tbl_medical WHERE tbl_medical.Name LIKE  '%" + txt_radiologist.Text + "%'", txt_rad_license, "License");

        }

        private void txt_bday_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Back || e.KeyCode == Keys.Delete)
            {
                dt_bday.Format = DateTimePickerFormat.Custom;
                dt_bday.CustomFormat = "00/00/0000";
            }
        }

        private void txt_bday_MouseDown(object sender, MouseEventArgs e)
        {
            dt_bday.Format = DateTimePickerFormat.Custom;
            dt_bday.CustomFormat = "MM/dd/yyyy";
        }

        private void dt_bday_ValueChanged(object sender, EventArgs e)
        {
            //if (dt_bday.Text != "" || dt_bday.Text != "0000-00-00 00:00:00" || dt_bday.Text != "00/00/0000")
            //{

            //    int age = DateTime.Now.Year - Convert.ToDateTime(dt_bday.Text).Year;
            //    txt_age.Text = age.ToString();

            //}
            //else
            //{ txt_age.Clear(); }

            if (dt_bday.Text != "" || dt_bday.Text != "0000-00-00 00:00:00" || dt_bday.Text != "00/00/0000")
            {
                int age_ = DateTime.Now.Year - dt_bday.Value.Year;
                txt_age.Text = age_.ToString();

                DateTime CurrentDate = DateTime.Parse(DateTime.Now.Date.ToShortDateString());
                int Age = CurrentDate.Year - dt_bday.Value.Year;

                if (CurrentDate.Month < dt_bday.Value.Month)
                {
                    Age--;
                }
                else if ((CurrentDate.Month == dt_bday.Value.Month) && (CurrentDate.Day < dt_bday.Value.Day))
                {

                    Age--;
                }
                this.txt_age.Text = Age.ToString();




            }
            else
            {
                txt_age.Clear();
            }


        }

        private void dt_result_Date_ValueChanged(object sender, EventArgs e)
        {
            SendKeys.Send("{RIGHT}");
        }




        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {


                var list = db.sp_Xray_SearchList("%", variables.RadiologyFormType);
                Cursor.Current = Cursors.WaitCursor;
                foreach (var i in list)
                {

                    Xray_model.Add(new Xray_Model
                    {
                        cn = i.cn,
                        papin = i.papin,
                        resultID = i.resultid,
                        patientName = i.PatientName,
                        resultDate = i.result_date
                    });

                }
                // sw.Close();

            }
            catch (Exception ex)
            {
                MessageBox.Show(this, string.Format("An error occured {0}", ex.Message), Properties.Settings.Default.SystemName.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation); return;

            }
            //finally
            //{

            //    if (ClassSql.dr != null) { ClassSql.dr.Close(); }

            //}
        }

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {


                var list = db.sp_XrayAdd("%");

                foreach (var i in list)
                {

                    xrayAdd_model.Add(new QueueSearchList_Model
                    {
                        cn = i.cn,
                        papin = i.papin,
                        PatientName = i.PatientName,
                        gender = i.gender

                    });
                }




            }
            catch (Exception ex)
            {
                MessageBox.Show(this, string.Format("An error occured {0}", ex.Message), Properties.Settings.Default.SystemName.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation); return;

            }

        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {

                if (!backgroundWorker2.IsBusy)
                { backgroundWorker2.RunWorkerAsync(); }
                if ((Application.OpenForms["frm_search_xray"] as frm_search_xray) != null)
                { (Application.OpenForms["frm_search_xray"] as frm_search_xray).FillDataGridView(); }

            }
            catch (Exception ex)
            {
                MessageBox.Show(this, string.Format("An error occured {0}", ex.Message), Properties.Settings.Default.SystemName.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation); return;

            }



            //(Application.OpenForms["frm_search_xray"] as frm_search_xray).lbl_notification.Visible = false;

        }

        private void frm_xray_Enter(object sender, EventArgs e)
        {
            //xrayAdd_model.Clear();
            //Xray_model.Clear();
            //if (!backgroundWorker1.IsBusy)
            //{
            //    backgroundWorker1.RunWorkerAsync();
            //}
        }

        private void button1_Click(object sender, EventArgs e)
        {
            MessageBox.Show(TemplatePath.basePath);
        }

        //



    }
}
