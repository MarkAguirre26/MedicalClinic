﻿using MedicalManagement.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace MedicalManagement
{

    public partial class frm_search_MedicalExamination : Form
    {
        //private string FilterBy;

        Main fmain;
        private const long BUTTON_DOWN_CODE = 0xa1;
        private const long BUTTON_UP_CODE = 0xa0;
        private const long WM_MOVING = 0x216;
        static bool left_button_down = false;

        public List<landbaseSearckList_Model> landbaseSearckList_model = new List<landbaseSearckList_Model>();
        public bool isOpen = false;
        public frm_search_MedicalExamination(Main maiin)
        {
            InitializeComponent();
            fmain = maiin;
        }

        private void cmd_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        protected override void DefWndProc(ref System.Windows.Forms.Message m)
        {
            //Check the state of the Left Mouse Button
            if ((long)m.Msg == BUTTON_DOWN_CODE)
                left_button_down = true;
            else if ((long)m.Msg == BUTTON_UP_CODE)
                left_button_down = false;

            if (left_button_down)
            {
                if ((long)m.Msg == WM_MOVING)
                {
                    //Set the forms opacity to 50% if user is moving
                    if (this.Opacity != 0.5)
                        this.Opacity = 0.5;
                }
            }

            else if (!left_button_down)
                if (this.Opacity != 1.0)
                    this.Opacity = 1.0;

            base.DefWndProc(ref m);
        }
        private void frm_search_Land_Load(object sender, EventArgs e)
        {
            Tool.DeleteTemporaryFilesFromTemplate(TemplatePath.basePath);

            isOpen = true;
            cbo_filter.Text = "Patient Name";

            txt_search.Select();

           //


            if (backgroundWorker1.IsBusy)
            {
                backgroundWorker1.CancelAsync();

            }
            backgroundWorker1.RunWorkerAsync();
        }



        private void cbo_filter_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmd_search_Click(object sender, EventArgs e)
        {
            this.Close();
            SelectItem();
        }
        

        void SelectItem()
        {
            if (dg_result.SelectedRows.Count >= 1)
            {
                this.Close();
                Cursor.Current = Cursors.WaitCursor;
                frm_MedicalExamination.Text_papin.Text = this.dg_result.SelectedRows[0].Cells[1].Value.ToString();
                frm_MedicalExamination.LabID.Text = this.dg_result.SelectedRows[0].Cells[2].Value.ToString();

                (Application.OpenForms["frm_MedicalExamination"] as frm_MedicalExamination).Search();
                //(Application.OpenForms["frm_MedicalExamination"] as frm_MedicalExamination).ClearAll();
                //(Application.OpenForms["frm_MedicalExamination"] as frm_MedicalExamination).Search_Patient();
                //(Application.OpenForms["frm_MedicalExamination"] as frm_MedicalExamination).Search_MedicalHistory();
                //(Application.OpenForms["frm_MedicalExamination"] as frm_MedicalExamination).Search_PhyExam();
                //(Application.OpenForms["frm_MedicalExamination"] as frm_MedicalExamination).Search_others();
                //(Application.OpenForms["frm_MedicalExamination"] as frm_MedicalExamination).search_Ancillary();
                //(Application.OpenForms["frm_MedicalExamination"] as frm_MedicalExamination).search_Recomendation();

                fmain.ts_add_land.Enabled = true; fmain.ts_edit_land.Enabled = true; fmain.ts_delete_land.Enabled = true; fmain.ts_save_land.Enabled = false; fmain.ts_search_land.Enabled = true; fmain.ts_print_land.Enabled = true; fmain.ts_cancel_land.Enabled = false;
                //

                Cursor.Current = Cursors.Default;


            }


        }

        public void FillDataGridView()
        {

            try
            {
              
                var list = landbaseSearckList_model.ToList();
                if (cbo_filter.Text == "Patient PIN")
                {

                    list = (from m in list where m.papin.StartsWith(txt_search.Text) select m).ToList();
                }
                else if (cbo_filter.Text == "Patient Name")
                {

                    list = (from m in list where m.patientName.StartsWith(txt_search.Text) select m).ToList();
                }
                dg_result.DataSource = list;




            }
            catch (Exception ex)
            {
                MessageBox.Show(this, string.Format("An error occured {0}", ex.Message), Properties.Settings.Default.SystemName.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation); return;

            }



        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {

            FillDataGridView();

        }

        private void txt_search_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (dg_result.SelectedRows.Count >= 1)
            { cmd_search.Enabled = true; }
            else
            { cmd_search.Enabled = false; }

        }

        private void dg_result_DoubleClick(object sender, EventArgs e)
        {
            SelectItem();
        }

        private void txt_search_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            { dg_result.Focus(); }

        }

        private void frm_search_Land_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            { SelectItem(); }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Search();
            txt_search.Clear();
            txt_search.Select();


        }

        private void patientInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dg_result.SelectedRows.Count >= 1)
            {
                frm_patient_Info info = new frm_patient_Info();
                info.Tag = dg_result.SelectedRows[0].Cells[1].Value.ToString();
                info.ShowDialog();


            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            this.Invoke(new MethodInvoker(delegate () {
              
                DataClasses1DataContext db = new DataClasses1DataContext(Properties.Settings.Default.MyConString);
                var list = db.sp_landbase_search("%").ToList();
                landbaseSearckList_model.Clear();
                foreach (var i in list)
                {


                    landbaseSearckList_model.Add(new landbaseSearckList_Model
                    {

                        cn = i.cn,
                        papin = i.papin,
                        resultID = i.resultid,
                        patientName = i.patientName,
                        resultDate = i.result_date,
                        recommendation = i.recommendation,
                        DateCreated = i.DateCreated.ToString()
                    });
                }

            }));
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        private void frm_search_Land_FormClosing(object sender, FormClosingEventArgs e)
        {
            isOpen = false;
        }

        private void cmd_refresh_Click(object sender, EventArgs e)
        {
          

        }

        private void backgroundWorker1_RunWorkerCompleted_1(object sender, RunWorkerCompletedEventArgs e)
        {
            FillDataGridView();
        }
    }
}
