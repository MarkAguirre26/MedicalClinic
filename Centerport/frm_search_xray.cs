﻿using MedicalManagement.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace MedicalManagement
{
    public partial class frm_search_xray : Form
    {
        //private string FilterBy;
        Main fmain;

        private const long BUTTON_DOWN_CODE = 0xa1;
        private const long BUTTON_UP_CODE = 0xa0;
        private const long WM_MOVING = 0x216;
        static bool left_button_down = false;
        public bool isOpen;
        DataClasses1DataContext db = new DataClasses1DataContext(Properties.Settings.Default.MyConString);
        public List<Xray_Model> Xray_model = new List<Xray_Model>();
        public frm_search_xray(Main maiin)
        {
            InitializeComponent();
            fmain = maiin;
        }

        


        void SelectItem()
        {
            if (dg_result.SelectedRows.Count >= 1)
            {
                this.Close();
                Cursor.Current = Cursors.WaitCursor;
                frm_xray.pin.Clear();
                frm_xray.pin.Text = this.dg_result.SelectedRows[0].Cells[1].Value.ToString();
                frm_xray.LabID.Clear();
                frm_xray.LabID.Text = this.dg_result.SelectedRows[0].Cells[2].Value.ToString();
                (Application.OpenForms["frm_xray"] as frm_xray).Tag = this.dg_result.SelectedRows[0].Cells[0].Value.ToString();


                (Application.OpenForms["frm_xray"] as frm_xray).ClearAll();
                (Application.OpenForms["frm_xray"] as frm_xray).Search_Patient();
                (Application.OpenForms["frm_xray"] as frm_xray).search_Medical();
                (Application.OpenForms["frm_xray"] as frm_xray).search_Xray();

                fmain.ts_printPreview_Xray.Enabled = true; fmain.ts_add_xray.Enabled = true; fmain.ts_edit_xray.Enabled = true; fmain.ts_delete_xray.Enabled = false; fmain.ts_save_xray.Enabled = false; fmain.ts_search_xray.Enabled = true; fmain.ts_print_xray.Enabled = true; fmain.ts_cancel_xray.Enabled = false;


                // frm_land.ts_add_land.Enabled = true; frm_land.ts_edit_land.Enabled = true; frm_land.ts_delete_land.Enabled = false; frm_land.ts_save_land.Enabled = false; frm_land.ts_cancel_land.Enabled = false; frm_land.ts_search_land.Enabled = true; frm_land.ts_print_land.Enabled = true;
                Cursor.Current = Cursors.Default;

            }


        }

        private void cbo_filter_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void txt_search_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.KeyChar = Char.ToUpper(e.KeyChar);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (dg_result.SelectedRows.Count >= 1)
            { cmd_search.Enabled = true; }
            else
            { cmd_search.Enabled = false; }
        }

        private void dg_result_DoubleClick(object sender, EventArgs e)
        {
            SelectItem();
        }

        private void cmd_cancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmd_search_Click(object sender, EventArgs e)
        {
            this.Close();
            SelectItem();
        }

        private void txt_search_TextChanged(object sender, EventArgs e)
        {

            FillDataGridView();
        }
        protected override void DefWndProc(ref System.Windows.Forms.Message m)
        {
            //Check the state of the Left Mouse Button
            if ((long)m.Msg == BUTTON_DOWN_CODE)
                left_button_down = true;
            else if ((long)m.Msg == BUTTON_UP_CODE)
                left_button_down = false;

            if (left_button_down)
            {
                if ((long)m.Msg == WM_MOVING)
                {
                    //Set the forms opacity to 50% if user is moving
                    if (this.Opacity != 0.5)
                        this.Opacity = 0.5;
                }
            }
            //
            else if (!left_button_down)
                if (this.Opacity != 1.0)
                    this.Opacity = 1.0;

            base.DefWndProc(ref m);
        }
        private void frm_search_xray_Load(object sender, EventArgs e)
        {
            isOpen = true;
            cbo_filter.Text = "Patient Name";           
            txt_search.Select();
            if (!backgroundWorker1.IsBusy)
            {
                backgroundWorker1.RunWorkerAsync();
            }



        }

        private void txt_search_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Down)
            { dg_result.Focus(); }
        
        }

        private void button1_Click(object sender, EventArgs e)
        {
          
            txt_search.Clear();
            txt_search.Select();

        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }


        public void FillDataGridView()
        {



            try
            {
              
                 var  list = Xray_model.ToList();            
             
                if (cbo_filter.Text == "Patient PIN") 
                {
                    
                     list = (from m in list where m.papin.StartsWith(txt_search.Text) select m).ToList();
      
                }
                else if (cbo_filter.Text == "Patient Name")
                {
                    list = (from m in list where m.patientName.StartsWith(txt_search.Text) select m).ToList();
                   
                }
                dg_result.DataSource = list;
                dg_result.Columns[0].Visible = false;
                dg_result.Columns[1].Visible = false;
                dg_result.Columns[2].Visible = false;
                dg_result.Columns[3].Width = 350;
                dg_result.Columns[4].Width = 150;
               




            }
            catch (Exception ex)
            {
                MessageBox.Show(this, string.Format("An error occured {0}", ex.Message), Properties.Settings.Default.SystemName.ToString(), MessageBoxButtons.OK, MessageBoxIcon.Exclamation); return;

            }
        }

        private void patientInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (dg_result.SelectedRows.Count >= 1)
            {
                frm_patient_Info info = new frm_patient_Info();
                info.Tag = dg_result.SelectedRows[0].Cells[1].Value.ToString();
                info.ShowDialog();


            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            this.Invoke(new MethodInvoker(delegate() {
              
                //
                var list = db.sp_Xray_SearchList("%", variables.RadiologyFormType);
                Cursor.Current = Cursors.WaitCursor;
                Xray_model.Clear();
                foreach (var i in list)
                {

                    Xray_model.Add(new Xray_Model
                    {
                        cn = i.cn,
                        papin = i.papin,
                        resultID = i.resultid,
                        patientName = i.PatientName,
                        resultDate = i.result_date
                    });

                }

            }));
        }

      
        private void frm_search_xray_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            { SelectItem(); }
        }

        private void frm_search_xray_FormClosing(object sender, FormClosingEventArgs e)
        {
            isOpen = false;
        }

        private void backgroundWorker1_RunWorkerCompleted_1(object sender, RunWorkerCompletedEventArgs e)
        {
            FillDataGridView();
        }
    }
}
