﻿
using System.Collections.Generic;
namespace MedicalManagement.Class
{
    public class Datas
    {
        public static string PatientName="";
        public static string address_1="";
        public static string contact_1="";
        public static string gender="";
        public static string marital_status="";
        public static string employer="";
        public static string ResultDate="";
        public static string PresentSymptoms="";
        public static string nearVision = "";
        public static string Tubercolosis="";
        public static string Asthma="";
        public static string Chickenpox="";
        public static string Pneumonia="";
        public static string Hypertension="";
        public static string HeartDisease="";
        public static string DiabetesMellitus="";
        public static string ThyroidDisorders="";
        public static string WoGlassOD = "";
        public static string WoGlassOS = "";
        public static string WglassOD = "";
        public static string WGlassOS = "";
        public static string EyeEarDisorders="";
        public static string NoseThroatDisorders="";
        public static string SkinDisease="";
        public static string CancerTumor="";
        public static string Musculoskeletal="";
        public static string GastritisUlcer="";
        public static string TyphoidParatyphoid="";
        public static string KidneyDisease="";
        public static string LiverDisease="";
        public static string Measles="";
        public static string SeizureDisorders="";
        public static string HeadachesMigraine="";
        public static string MentalDisorders="";
        public static string SexuallyTransmitted="";
        public static string GeneticDisorders="";
        public static string Other="";
        public static string FamilyMedicalHistory="";
        public static string OperationsandAccidents="";
        public static string Allergies="";
        public static string PersonaSocialHistory="";
        public static string Smoker="";
        public static string AlcoholDrinker="";
        public static string HEIGHT="";
        public static string WEIGHT="";
        public static string BP="";//
        public static string PULSE="";
        public static string SATISFACTORY_HEARING="";
        public static string BODY_BUILD="";
        public static string ISHIHARA="";
        public static string HEAD_NECK_SCALP_TAG="";
        public static string EYES_TAG="";
        public static string EARS_EARDRUM_TAG="";
        public static string NOSE_SINUSES_TAG="";
        public static string MOUTH_THROAT_TAG="";
        public static string CHEST_BREAST_AXILLA_TAG="";
        public static string LUNGS_TAG="";
        public static string HEART_TAG="";
        public static string ABDOMEN_TAG="";
        public static string BACK_FLANK_TAG="";
        public static string INGUINALS_GENITALS_TAG = "";
        public static string EXTREMITIES_TAG="";
        public static string SKIN_TAG="";
        public static string DENTAL_TAG="";
        public static string HEAD_NECK_SCALP="";
        public static string EYES="";
        public static string EARS_EARDRUM="";
        public static string NOSE_SINUSES="";
        public static string MOUTH_THROAT="";
        public static string CHEST_BREAST_AXILLA="";
        public static string LUNGS="";
        public static string HEART="";
        public static string ABDOMEN="";
        public static string BACK_FLANK="";
        public static string INGUINALS_GENITALS="";
        public static string EXTREMITIES="";
        public static string SKIN="";
        public static string DENTAL="";
        public static string OralProphylaxis = "";
        public static string Extraction = "";
        public static string Filling = "";
        public static string DentalLowerLeft="";
        public static string lmp="";
        public static string obScore="";
        public static string Interval="";
        public static string Duration="";
        public static string Dysmenorrhea="";
        public static string Hemoglobin="";
        public static string Hematocrit="";
        public static string RBCCount="";
        public static string WBCCount="";
        public static string Neutrophils="";
        public static string Lymphocytes="";
        public static string Eosonophils="";
        public static string Monocytes="";
        public static string Basophils="";
        public static string Platelet="";
        public static string Color_URINALYSIS="";
        public static string Transparency="";
        public static string pH="";
        public static string SpecificGravity="";
        public static string Sugar="";
        public static string Albumin="";
        public static string Bacteria="";
        public static string wbc="";
        public static string rbcURINALYSIS="";
        public static string EpithelialCells="";
        public static string MucousThreads="";
        public static string AmorphousUrates="";
        public static string Casts="";
        public static string Crystals="";
        public static string PregnancyTest="";
        public static string color_FECALYSIS="";
        public static string Consistency="";
        public static string PusCells="";
        public static string rbc="";
        public static string ova="";
        public static string radiologyNo="";
        public static string impressionXRAY="";
        public static string findingsXRAY="";
        public static string ecgResult="";
        public static string ecgImpresion="";
        public static string FARwoGlassesOD="";
        public static string FARwoGlassesOS="";
        public static string FARWGlassesOD="";
        public static string FARWGlassesOS="";
        public static string NearVision="";
        public static string remarks="";
        public static string recommendations="";
        public static string DentalOthers = "";
        public static string hbsag  = "";
        public static string abo = "";
        public static string rh = "";
        public static string fbs = "";
        public static string rbs = "";
        public static string othersPhy = "";
        public static string HepaBTest = "";
        public static string PendingValue = "";
        public static string UnfitValue = "";
        public static string AnusTag = "";
        public static string Anus = "";
        







        public static string classificationA = "";
        public static string ISHIHARA_U = "";


        
        
        //
        


//        B13,	PatientName
// B14, address_1
// B15, 	contact_1
//H13, gender
//H14, 	marital_status
//R13, employer
//R14, 	ResultDate
//D18, PresentSymptoms
//D48 , 	nearVision
//E21, Tubercolosis
//E22, 	Asthma
//E23, Chickenpox
//E24, 	Pneumonia
//E25, Hypertension
//E26, 	HeartDisease
//E27, DiabetesMellitus
//E28, 	ThyroidDisorders
//E45, WoGlassOD
//E46, 	WoGlassOS
//H45, WglassOD
//H46, 	WGlassOS
//J21, EyeEarDisorders
//J22, 	NoseThroatDisorders
//J23, SkinDisease
//J24, 	CancerTumor
//J25, Musculoskeletal
//J26, 	GastritisUlcer
//J27, TyphoidParatyphoid
//J28, 	KidneyDisease
//W21, LiverDisease
//W22, 	Measles
//W23, SeizureDisorders
//W24, 	HeadachesMigraine
//W25, MentalDisorders
//W26, 	SexuallyTransmitted
//W27, GeneticDisorders
//W28, 	Other
//D30, FamilyMedicalHistory
//D31,	OperationsandAccidents
// D32, Allergies
// D35, 	PersonaSocialHistory
//I34, Smoker
//I35, 	AlcoholDrinker
//D38, HEIGHT
// G39,	WEIGHT
// D40, BP
//D41,	PULSE
// D42, SATISFACTORY_HEARING
// G38,	BODY_BUILD
// D50, ISHIHARA
// P39, 	HEAD_NECK_SCALP_TAG
//P40, EYES_TAG
//P41,	EARS_EARDRUM_TAG
// P42, NOSE_SINUSES_TAG
// P43,	MOUTH_THROAT_TAG
// P44, CHEST_BREAST_AXILLA_TAG
//P45,	LUNGS_TAG
//P46, HEART_TAG
//P47, 	ABDOMEN_TAG
//P48, BACK_FLANK_TAG
//P49, 	INGUINALS_GENITALS_TAG
//P50, EXTREMITIES_TAG
//P51, 	SKIN_TAG
//P52, DENTAL_TAG //I put N by default in frm_MedicalExamination.cs
// R39,	HEAD_NECK_SCALP
//R40, EYES
//R41, 	EARS_EARDRUM
//R42, NOSE_SINUSES
//R43, 	MOUTH_THROAT
//R44, CHEST_BREAST_AXILLA
//R45, 	LUNGS
//R46, HEART
//R47, 	ABDOMEN
//R48, BACK_FLANK
//R49, 	INGUINALS_GENITALS
//R50, EXTREMITIES
//R51, 	SKIN
//R52, DENTAL
//E53, 	DentalUpperRight
//E56, DentalUpperLeft
//E54, 	DentalLowerRight
//E57, DentalLowerLeft
//I54, 	lmp
//I55, obScore
//I56, 	Interval
//I57, Duration
//I58, 	Dysmenorrhea
//D64, Hemoglobin
//D66, 	Hematocrit
//D68, RBCCount
//D70,	WBCCount
// D73, Neutrophils
//D74, 	Lymphocytes
//D75, Eosonophils
//D76, 	Monocytes
//D77, Basophils
//D78, 	Platelet
//I64, Color_URINALYSIS
//I65, 	Transparency
//I66, pH
//I67, 	SpecificGravity
//I68, Sugar
//I69, 	Albumin
//I70, Bacteria
//I71, 	wbc
//I72, rbcURINALYSIS
//I73, 	EpithelialCells
//I74, MucousThreads
//I75, 	AmorphousUrates
//I76, Casts
//I77, 	Crystals
//I78, PregnancyTest
//V64, 	color_FECALYSIS
//V65, Consistency
//V66, 	PusCells
//V67, rbc
//V68, 	ova
//B82, radiologyNo
//B87, 	impressionXRAY
//B84, findingsXRAY
//M84, 	ecgResult
//M87, ecgImpresion
//E45, 	FARwoGlassesOD
//E46, FARwoGlassesOS
//H45, 	FARWGlassesOD
//H46, FARWGlassesOS
//D48, 	NearVision
//H93, remarks
//H94, 	recommendations
//H91, EvaluationClassOnly
//I92, 	Evaluation
//R52 DentalOthers
//    classificationA

//    ISHIHARA_U






        //Patient

        public static string place_of_birth;
        public static string nationality;
        //public static string address_1;
        //public static string employer;
        //public static string gender;
        public static string position;
        public static string religion;
        //public static string marital_status;
        //public static string contact_1;
        public static string birthdate;
        //HISTORY
        //public static string Allergies;
        public static string AnemiaBleeding;
        //public static string Asthma;
        public static string BloodProblem;
        //public static string CancerTumor;
        //public static string Chickenpox;
        public static string Clotting;
        public static string Diabetes;
        //public static string DiabetesMellitus;
        public static string Epilepsy;
        //public static string EyeEarDisorders;
        //public static string GastritisUlcer;
        //public static string GeneticDisorders;
        public static string GermanMeasles;
        //public static string HeadachesMigraine;
        //public static string HeartDisease;
        public static string Hepatitis;
        public static string Hernia;
        //public static string Hypertension;
        //public static string KidneyDisease;
        //public static string LiverDisease;
        //public static string Measles;
        //public static string MentalDisorders;
        //public static string Musculoskeletal;
        //public static string NoseThroatDisorders;
        //public static string Pneumonia;
        public static string PsychologicalDisorder;
        public static string PTB;
        //public static string SeizureDisorders;
        //public static string SexuallyTransmitted;
        //public static string SkinDisease;
        //public static string ThyroidDisorders;
        //public static string Tubercolosis;
        //public static string TyphoidParatyphoid;
        public static string Ulcers;
        public static string Vertigo;
        //public static string Other;
        //public static string PresentSymptoms;
        //public static string FamilyMedicalHistory;
        //public static string OperationsandAccidents;
        //public static string PersonaSocialHistory;
        //public static string Smoker;
        public static string NoOfPackDay;
        //public static string AlcoholDrinker;
        public static string NoOfYear;
        //PHYSICAL EXAM
        //public static string HEIGHT;
        //public static string WEIGHT;
        //public static string BP;
        //public static string PULSE;
        public static string RESPIRATION;
        //public static string BODY_BUILD;
        public static string FAR_OD_U;
        public static string FAR_OD_C;
        public static string FAR_OS_U;
        public static string FAR_OS_C;
        public static string NEAR_ODJ_U;
        public static string NEAR_ODJ_C;
        public static string NEAR_OSJ_U;
        public static string NEAR_OSJ_C;
        //public static string ISHIHARA_U;
        //public static string ISHIHARA_C;
        public static string HEARING_AD;
        public static string HEARING_AS;
        public static string SPEECH;
        public static string CONVERSATIONAL_AD;
        public static string CONVERSATIONAL_AS;
        //public static string SATISFACTORY_HEARING;
        public static string SATISFACTORY_SIGHT_AID;
        public static string SATISFACTORY_SIGHT_UNAID;
        public static string SATISFACTORY_PSYCHO;
        public static string VISUAL_AIDS;
        public static string FIT_FOR_LOOKOUT;
        public static string HEARING_RIGHT;
        public static string HEARING_LEFT;
        public static string CLARITY_OF_SPEECH;
        public static string VISUAL_AIDS_REQUIRED;
        public static string BP_DIASTOLIC;
        public static string RHYTHM;
        public static string VISUAL_AIDS_WORN;
        public static string COLOR_VISION_DATE_TAKEN;
        public static string UNAIDED_HEARING_SATISFACTORY;
        public static string IDENTITY_CONFIRMED;
        //public static string DentalUpperRight;
        //public static string DentalUpperLeft;
        //public static string DentalLowerRight;
        //public static string DentalLowerLeft;
        //public static string lmp;
        //public static string obScore;
        //public static string Interval;
        //public static string Duration;
        //public static string Dysmenorrhea;
        //public static string OralProphylaxis;
        //public static string Fillings;
        //public static string Extraction;
        public static string obOther;
        //OTHERS
        //public static string SKIN_TAG;
        //public static string SKIN;
        //public static string HEAD_NECK_SCALP_TAG;
        //public static string HEAD_NECK_SCALP;
        //public static string EYES_TAG;
        //public static string EYES;
        public static string PUPILS_TAG;
        public static string PUPILS;
        //public static string EARS_EARDRUM_TAG;
        //public static string EARS_EARDRUM;
        //public static string NOSE_SINUSES_TAG;
        //public static string NOSE_SINUSES;
        //public static string MOUTH_THROAT_TAG;
        //public static string MOUTH_THROAT;
        //public static string NECK_LN_THYROID_TAG;
        //public static string NECK_LN_THYROID;
        //public static string CHEST_BREAST_AXILLA_TAG;
        //public static string CHEST_BREAST_AXILLA;
        //public static string LUNGS_TAG;
        //public static string LUNGS;
        //public static string HEART_TAG;
        //public static string HEART;
        //public static string ABDOMEN_TAG;
        //public static string ABDOMEN;
        //public static string BACK_FLANK_TAG;
        //public static string BACK_FLANK;
        //public static string ANUS_RECTUM_TAG;
        //public static string ANUS_RECTUM;
        public static string GU_SYSTEM_TAG;
        public static string GU_SYSTEM;
        //public static string INGUINALS_GENITALS_TAG;
        //public static string INGUINALS_GENITALS;
        public static string REFLEXES_TAG;
        public static string REFLEXES;
        //public static string EXTREMITIES_TAG;
        //public static string EXTREMITIES;
        //public static string DENTAL;
        //public static string DENTAL_TAG;


        //HEMATOLOGY
        //public static string Hemoglobin;
        //public static string Hematocrit;
        //public static string RBCCount;
        //public static string WBCCount;
        //public static string Neutrophils;
        //public static string Lymphocytes;
        //public static string Eosonophils;
        //public static string Monocytes;
        //public static string Basophils;
        //public static string Platelet;
        public static string NormalHEMATOLOGY;
        public static string FindingsHEMATOLOGY;
        //URINALYSIS
        //public static string Color_URINALYSIS;
        //public static string Transparency;
        //public static string pH;
        //public static string SpecificGravity;
        //public static string Sugar;
        //public static string Albumin;
        //public static string Bacteria;
        //public static string wbc;
        //public static string rbcURINALYSIS;
        //public static string EpithelialCells;
        //public static string MucousThreads;
        //public static string AmorphousUrates;
        //public static string Casts;
        //public static string Crystals;
        //public static string PregnancyTest;
        //public static string ResultDate;
        public static string NormalURINALYSIS;
        public static string FindingsURINALYSIS;
        //FECALYSIS
        //public static string color_FECALYSIS;
        //public static string Consistency;
        //public static string PusCells;
        //public static string rbc;
        //public static string ova;
        public static string NormalFECALYSIS;
        public static string FindingsFECALYSIS;
        public static string otherFECALYSIS;
        //XRAY
        //public static string radiologyNo;
        //public static string findingsXRAY;
        public static string examinationXRAY;
        //public static string impressionXRAY;
        //ECG

        //RESULT MAIN
        //  public static string Evaluation;
        // public static string remarks;
        // public static string recommendation;

        public static string xrayRemark;






    }
}
